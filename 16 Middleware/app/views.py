from django.shortcuts import render,HttpResponse
from django.template.response import TemplateResponse
# Create your views here.
def home(request):
    print("i am view")
    return HttpResponse("Home Page")

def excp(request):
    print('exception view')
    b=10/0
    return HttpResponse('this exception')

def user_info(request):
    print('user info view')
    context={'name':'Ritesh'}
    return TemplateResponse(request,'user.html',context)