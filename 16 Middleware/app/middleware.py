# def my_middleware(get_response):
#     print('One Time Intialization')
#     def my_func(request):
#         print('This is before view')
#         response=get_response(request)
#         print('this is after view')
#         return response
#     return my_func

class Middleware:
    def __init__(self,get_response):
        self.get_response=get_response
        print('One Time Intialization')

    def __call__(self,request):
        print("before view")
        response=self.get_response(request)
        print("after view")
        return response
    