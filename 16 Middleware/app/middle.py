from typing import Any
from django.shortcuts import HttpResponse

class MyProcessMiddleware:
    def __init__(self,get_response):
        self.get_response=get_response

    def __call__(self,request):
        response=self.get_response(request)
        return response

    def process_view(request,*args,**kwargs):
        print("process view before view")
        return HttpResponse("befor view")
        # return None

class MyExceptionMiddleware:
    def __init__(self,get_response):
        self.get_response=get_response

    def __call__(self,request):
        response=self.get_response(request)
        return response

    def process_exception(sef,request,exception):
        print("Exception Occured")
        msg=exception
        return HttpResponse(msg)

class MyTemplateResponseMiddleware:
    def __init__(self,get_response):
        self.get_response=get_response

    def __call__(self,request):
        response=self.get_response(request)
        return response

    def process_template_response(sef,request,response):
        print("Process Template Response From Middleware")
        response.context_data['name']='Nilesh'
        return response
        # return None
        