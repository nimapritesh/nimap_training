from django.db import models
from django.core.validators import FileExtensionValidator

# Create your models here.


class FAQS(models.Model):
    id = models.IntegerField(primary_key=True)
    questions = models.CharField(max_length=200)
    answer = models.TextField(max_length=300)
    attachment = models.FileField(blank=True, upload_to='pdffile', validators=[
                                  FileExtensionValidator(allowed_extensions=["pdf"])])


class File(models.Model):
    title = models.CharField(max_length=50)
    image_file = models.ImageField(upload_to='image', blank=True)


def __str__(self):
    return self.title
