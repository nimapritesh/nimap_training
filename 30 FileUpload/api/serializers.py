from rest_framework import serializers
from .models import File, FAQS


class FaqsSerializers(serializers.ModelSerializer):
    class Meta:
        model = FAQS
        fields = "__all__"


class FileSerializers(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"
