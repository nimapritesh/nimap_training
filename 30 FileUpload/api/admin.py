from django.contrib import admin
from .models import File, FAQS
# Register your models here.


class FileAdmin(admin.ModelAdmin):
    list_display = ('title', 'image_file')


admin.site.register(File, FileAdmin)


class FaqsAdmin(admin.ModelAdmin):
    list_display = ('id', 'questions', 'answer', 'attachment')


admin.site.register(FAQS, FaqsAdmin)
