from django.urls import path
from api import views

urlpatterns = [
    path('file/', views.GetFile.as_view()),
    path('file/<int:pk>', views.Update.as_view()),
    path('faqs/', views.ViewFaqs.as_view()),
    path('faqs/<int:pk>', views.UpdateFaqs.as_view()),
]
