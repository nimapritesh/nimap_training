from django.shortcuts import render
from .models import File, FAQS
from .serializers import FileSerializers, FaqsSerializers
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

# Create your views here.


class ViewFaqs(ListCreateAPIView):
    queryset = FAQS.objects.all()
    serializer_class = FaqsSerializers

    @method_decorator(cache_page(1))
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class UpdateFaqs(RetrieveUpdateDestroyAPIView):
    queryset = FAQS.objects.all()
    serializer_class = FaqsSerializers


class GetFile(ListCreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializers


class Update(RetrieveUpdateDestroyAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializers
