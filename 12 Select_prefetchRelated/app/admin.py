from django.contrib import admin
from .models import Customer,Order,Products
# Register your models here.
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['id','name','email']

admin.site.register(Customer, CustomerAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = ['id','customer','product_name','product_type','product_price']

admin.site.register(Products, ProductAdmin)

class OrderAdmin(admin.ModelAdmin):
    list_display = ['id','ordered_date','complete','transaction_id']

admin.site.register(Order, OrderAdmin)


