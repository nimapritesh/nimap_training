
##1. Write a Python script to sort (ascending and descending) a dictionary by value.
##import operator
##d = {1: 2, 3: 4, 4: 3, 2: 1, 0: 0}
##print('Original dictionary : ',d)
##sd = sorted(d.items(), key=operator.itemgetter(1))
##print('Ascending order : ',sd)
##sd = dict( sorted(d.items(), key=operator.itemgetter(1),reverse=True))
##print('Descending order : ',sd)

##2. Write a Python program to add a key to a dictionary
##d={"name":"ritesh",
##   "age":22}
##d.update({'area':'aarey'})
##
##d["addres"]="Goregaon"
##print(d)

##3. Write a Python program to concatenate following dictionaries to create a new one.
##d1={1:2,2:3,4:5,5:6}
##d2={6:7,7:8,8:9,9:10}
##d3={}
##for d in (d1,d2):
##    d3.update(d)
##print(d3)

##4. Write a Python program to check whether a given key already exists in a dictionary.

##d={'a':1,'b':2,'c':3}
##n=input("enter key: ")
##if n in d:
##    print('yes entered key is present in d')
##else:
##    print('No entered key is not present in d')

##5. Write a Python program to iterate over dictionaries using for loops.
##
##d={'fruit':'apple',
##   'price':230,
##   'origin':'kashmir'}
##for i,j in d.items():
##    print(i,j)

##6. Write a Python script to generate and print a dictionary that contains a number (between 1 and n) in the form (x, x*x).

##d={2:2*2,3:3*3,4:4*4,5:5*5,6:6*6}
##print(d)

##n=int(input("enter limit: "))
##d={}
##for x in range(1,n+1):
##    d[x]=x*x
##print(d)

##7.Write a Python script to print a dictionary where the keys are numbers
##between 1 and n (both included) and the values are square of keys.

##8.Write a Python script to merge two Python dictionaries.
##d1={"apple":3,"banana":4,"mango":5,"kiwi":1}
##d2={"carrot":1,"onion":3,"ginger":2}
##d3={}
##for i in (d1,d2):
##    d3.update(i)
##print(d3)

##10. Write a Python program to sum all the items in a dictionary.

##d={1:2,3:4,5:6,7:8,9:10,10:11}
##print(sum(d.values()))
##sum1=0
##for i in d.values():
##    sum1=sum1+i
##print(sum1)

##11. Access dictionary key’s element by index.
##d={1:2,3:4,5:6,7:8,9:10,10:11}
##print(list(d)[0])
##print(list(d)[1])
##print(list(d)[2])
##print(list(d)[3])
##print(list(d)[4])
##print(list(d)[5])


##12. Drop empty Items from a given Dictionary
##a={"Name":"Ritesh","age":"",
##   "address":"Goregaon","language":""}
##new_d={}
##for i,j in a.items():
##    if j:
##        new_d[i]=j
##print(new_d)

##13. Create a dictionary of keys a, b, and c

##d={'a':'','b':'','c':3}
##print(d)

##num=dict(A=list(range(1,11)),B=list(range(11,21)),c=list(range(20,31)))
##for k,v in num.items():
##    print(k,v)

##Match key values in two dictionaries

##A = {'Tamil': 92, 'English': 56, 'Maths': 88, 'Sceince': 97, 'Social': 89}
##B= {'Tamil': 78, 'English': 56, 'Maths': 88, 'Sceince': 97, 'Social': 56}
##print(set(A.items()))
##for k,v in set(A.items()) & set(B.items()):
##    print(k,v)


##15. Sort Counter by value
##import operator
##A = {'Tamil': 92, 'English': 56, 'Maths': 88, 'Sceince': 97, 'Social': 89}
##opt={}
##temp=[]
##for i in A.values():
##    temp.append(i)
##temp.sort(reverse=True)
##for i in temp:
##    opt[i]=A[i]
##print(opt)

##d =  {'M1': [67,79,90,73,36], 'M2': [89,67,84], 'M3': [82,57]}
##tot = sum(map(len, d.values()))
##print("Number of Items in a Dictionary :",tot)
##a=list(d.values())
##count=0
##for i in a:
##    for j in i: 
##     count=count+1
##print(count)

##17. Write a Python program to check multiple keys exists in a dictionary.

##d={"apple":1,"mango":2,"chiku":3,"grapes":4,"apple":3,"chiku":6}
##new={}
##for k in d.keys():
##    if k in d:
##        new=k
##print(new)

##18. Write a Python program to print a dictionary line by line

##sub = {"Sam":{"M1":89,"M2":56,"M3":89},
##        "Suresh":{"M1":49,"M2":96,"M3":89}}
##for i in sub:
##    print(sub[i])
##    for j in sub[i]:
##        print(j,sub[i][j])

##19. Write a Python program to Convert two lists into a dictionary

##keys=[1,2,3,4,5]
##values=["a","b","c","d","e"]
##res={}
####d=dict(zip(keys,values))
####print(d)
##for k in keys:
##    for v in values:
##     res[k]=v
##     values.remove(v)
##     break
##            
##print(res)

##21. Write a Python program to Initialize dictionary with default values

##stu = ["Siva", "Sam", "Ram", "Pooja"]
##defaults = {"designation": "Student", "Department": "BCA"}
##
## 
##result=dict.fromkeys(stu,defaults)
##print(result)
 
##22. Create a dictionary by extracting the keys from a given dictionary
##d={"a":1,"b":2,"c":3}
##keys=["a","b"]
##n={}
##for k, v in d.items():
##    if k in keys:
##        n[k] = v
##print(n)
 
 
##23. Write a Python program to Delete a list of keys from a dictionary

##student = {
##"Name": "Tara","RollNo":130046, "Mark": 458, "Age":16,"Gender":"Female","City": "Chennai"}
##keys=['Mark','City']
##for k in keys:
##    student.pop(k)
##print(student)
 
##24. Write a Python program to Rename key of a dictionary
##student = {
##	"Name": "Tara","RollNo":130046, "Mark": 458, "Age":16,}
##student['Regno']=student.pop('RollNo')
##student['total']=student.pop('Mark')
##print(student)

####25. Write a Python program to Replace words from Dictionary
##val="tutor joe's computer education"
##dic={'computer':'software','education':'solution'}
##
##word=val.split()
##res=[]
##for w in word:
##    res.append(dic.get(w,w))
##res=' '.join(res)
##print(res)

##26. Write a program to print only keys of a dictionary
##dic={'a':11,'b':12,'c':13,'d':14}
##print(dic.keys())
##for k in dic.keys():
##    print(k)

##27. Write a program to print values of dictionary.
##dic={'a':11,'b':12,'c':13,'d':14}
##print(dic.values())
##for v in dic.values():
##    print(v)

##28. Write a program to sort dictionary by values (Ascending/ Descending).

##dic={1:5,3:1,5:3,9:9,8:4}
##print(sorted(dic.values()))


##29. Write a program to get the maximum and minimum value of dictionary.

##marks={"m1":78 , "m2":89 , "m3":64 , "m4":35 , "m5":71}
##v=marks.values()
##l=list(v)
##max1=l[0]
##for i in l:
##    if i>max1:
##        max1=i
##print(max1)

##ororororororor
##v = marks.values()
##maxi = max(v)
##mini = min(v)
## 
##print("Maximum :",maxi)
##print("Minimum :",mini)

####30. Write a program to sum all the values of a dictionary.
##marks={"m1":92 , "m2":56 , "m3":88 , "m4":97 , "m5":89}
##v=marks.values()
##l=list(v)
##sum1=0
##for i in l:
##    sum1=sum1+i
##print(sum1)

##ororororororororororororor
##total = []
##for i in marks.values():
##	total.append(i)
## 
##print("Sum of Values :",sum(total))
 
