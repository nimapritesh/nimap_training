from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Task(models.Model):
    owner_id=models.ForeignKey(User,on_delete=models.CASCADE)
    task_name=models.CharField(max_length=30)
    status=models.IntegerField(default=0)
    created_at=models.DateTimeField(auto_now=True)
    updated_at=models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.task_name} {self.created_at} {self.updated_at}"

class Subtask(models.Model):
    task_id=models.ForeignKey(Task, on_delete=models.CASCADE)
    subtask_name=models.CharField(max_length=30)
    created_at=models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.task_id} {self.subtask_name} {self.created_at}"