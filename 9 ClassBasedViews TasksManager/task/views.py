from django.shortcuts import render,redirect
from task.models import Task,Subtask
from django.views import View
from django.views.generic.edit import UpdateView

from django.contrib.auth.models import User
# Create your views here.
class Gettask(View):
 def get(self,request):
    get_task=Task.objects.all()
    context={'task':get_task}
    return render (request,'list.html',context)

class Getsubtask(View):
 def get(self,request,id):
        get_task=Task.objects.get(id=id)
        get_subtask=Subtask.objects.filter(task_id=id)
        subtask={'subtask':get_subtask,'task':get_task}
        return render(request,'subtask.html',subtask)
    
 def post(self,request,id):
        task=Task.objects.get(id=id)
        subtask_name=request.POST.get('subtask_name')
        subtask=Subtask(subtask_name=subtask_name,task_id=task)
        subtask.save()
        return redirect('/')

class Createtask(View):
 def post(self,request):
    if request.method=="POST":
        task_name=request.POST.get('task_name')
        status=request.POST.get('status')
        if status=='complete':
            status=1
        else:
            status=0
        owner_instance=User.objects.get(username="manager")
        data=Task(task_name=task_name,status=status,owner_id=owner_instance)
        data.save()
        return redirect("/")

    return render(request,'createtask.html')

class Update(UpdateView):
 def post(self,request):
    id=request.POST.get('id')
    task_name=request.POST.get('task_name')
    status=request.POST.get('status')
    task_instance=Task.objects.get(id=id)
    task_instance.task_name=task_name
    if status=='complete':
        task_instance.status=1
    else:
        task_instance.status=0
    task_instance.save()
    return redirect("/")

# def delete(request,pk):
#     task=Task.objects.get(int=id)

#     if request.method=='POST':
#         task.delete()
#         return redirect('/')
#     context={'task':task}
#     return render(request,'list.html',context)