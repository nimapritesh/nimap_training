from rest_framework import serializers

class StudentSerializer(serializers.Serializer):
    id=serializers.IntegerField()
    stu_roll = serializers.IntegerField()
    first_name = serializers.CharField(max_length=30)
    last_name = serializers.CharField(max_length=30)

class DetailsSerializers(serializers.Serializer):
    id=serializers.IntegerField()
    stu_name = serializers.CharField(max_length=30)
    phone = serializers.CharField(max_length=30)
    email= serializers.CharField(max_length=50)

#For Creation
'''def create(self,validate):
    return Canditate.objects.create(**validate_data)'''
