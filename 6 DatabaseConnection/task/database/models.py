from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Canditates(models.Model):
    stu_roll = models.IntegerField()
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    def __str__(self):
     return f"{self.stu_roll} {self.first_name} {self.last_name}"
    
class DetailsofCandidate(models.Model): # StudentDetail
    #stu_roll  =  models.ForeignKey(Student, null=True, on_delete=models.CASCADE) 
    stu_name = models.CharField(max_length=30)
    phone = models.CharField(max_length=30)
    email= models.CharField(max_length=50)

    def __str__(self):
     return f"{self.stu_name} {self.phone} {self.email}"
