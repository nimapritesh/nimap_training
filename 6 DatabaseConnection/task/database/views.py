from django.shortcuts import render
from .models import Canditates,DetailsofCandidate
from .serializers import StudentSerializer,DetailsSerializers
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse,JsonResponse
import io
from rest_framework.parsers import JSONParser
# Create your views here.
def student_info(request,pk):
    student=Canditates.objects.get(id=pk) #2 Shivam Jadhav
    serializer=StudentSerializer(student) #Converted into Python Object StudentSerializer(<Canditate: 2 Shivam Jadhav>):
    #json_data=JSONRenderer().render(serializer.data) #Render into json format '{"stu_roll":2,"first_name":"Shivam","last_name":"Jadhav"}
    #return HttpResponse(json_data,content_type='application/json')
    return JsonResponse(serializer.data)
def student_all_details(request):
    details=DetailsofCandidate.objects.all()
    serializer1=DetailsSerializers(details,many=True)
    json=JSONRenderer().render(serializer1.data)
    return HttpResponse(json,content_type='application/json')

def candidate_create(request):
    json_data=request.body
    stream=io.BytesIO(json_data)
    python_data=JSONParser().parse(stream)
    serializer=Canditates(python_data)
    if serializer.is_valid():
        serializer.save()
        res={'msg':'created'}
        json_data=JSONRenderer().render(res)
        return HttpResponse(json_data,content_type='application/json')



