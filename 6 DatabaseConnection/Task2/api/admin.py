from django.contrib import admin
from api.models import RailwayForm

# Register your models here.
class NameAdmin(admin.ModelAdmin):
    list_display = ('id','passanger_name','email_id','contact_no','address')

admin.site.register(RailwayForm, NameAdmin)


