from django.shortcuts import render
from .serializers import RailwayFormSerializer
from .models import RailwayForm
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response

# Create your views here.
@api_view(['GET','POST'])
def fillform(request):
 if request.method=='GET':
    queryset=RailwayForm.objects.all()
    serializers=RailwayFormSerializer(queryset,many=True)
    return Response(serializers.data)
 #return Response
 elif request.method=="POST" :
   serializers=RailwayFormSerializer(data=request.data)
   if serializers.is_valid():
     serializers.save()
   return Response(serializers.data)
 
@api_view(['GET','PUT','PATCH','DELETE'])
def all_action(request,pk):
   if request.method=='GET':
     queryset=RailwayForm.objects.get(id=pk)
     serializers=RailwayFormSerializer(queryset)
     return Response(serializers.data)
   
   elif request.method=='PUT':
     form_update=RailwayForm.objects.get(id=pk)
     serializers=RailwayFormSerializer(instance=form_update,data=request.data)
     if serializers.is_valid():
       serializers.save()
     return Response(serializers.data)
   
   elif request.method=='PATCH':
     form_update=RailwayForm.objects.get(id=pk)
     serializers=RailwayFormSerializer(instance=form_update,data=request.data)
     if serializers.is_valid():
       serializers.save()
     return Response(serializers.data)
   
   elif request.method == "DELETE":
        form_update = RailwayForm.objects.get(id=pk)
        form_update.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)
   
     