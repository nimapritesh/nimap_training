from django.db import models

# Create your models here.
class RailwayForm(models.Model):
    passanger_name=models.CharField(max_length=30)
    email_id = models.CharField(max_length=20)
    contact_no=models.IntegerField()
    address=models.CharField(max_length=50)

def __str__(self):
        return f"{self.passanger_name} {self.email_id} {self.contact_no} {self.address}"