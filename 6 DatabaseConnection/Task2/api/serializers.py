from rest_framework import serializers
from .models import RailwayForm

class RailwayFormSerializer(serializers.ModelSerializer):  
    class Meta:
        model=RailwayForm
        fields = ('id','passanger_name','email_id','contact_no','address')
