from http import client
from mains.models import Clients, Projects
from rest_framework.response import Response
from .serializers import ProjectSerializer, ClientsSerializer, ClientSerializer
from rest_framework.views import APIView

#Clients api endpoints
class Getclients(APIView):

 def get(self,request):
    client_all = Clients.objects.all()
    client_ser = ClientSerializer(client_all, many= True)
    return Response(client_ser.data)
 
 def post(self,request):
        client_ser = ClientSerializer(data=request.data)
        if client_ser.is_valid():
            client_ser.save(created_by=request.user)        
        return Response(client_ser.data)



class Clientdetails(APIView):
 def get(self,request, pk):
        client = Clients.objects.get(id=pk)
        print(client)
        client_ser = ClientsSerializer(client)
        return Response(client_ser.data)

 def put(self,request,pk):
        client_update = Clients.objects.get(id=pk)
        client_ser = ClientSerializer(instance= client_update, data=request.data)
        if client_ser.is_valid():
            client_ser.save()        
        return Response(client_ser.data)

 def delete(self,request,pk):
        client_update = Clients.objects.get(id=pk)
        client_update.delete()
        return Response(status ="HTTP_204_NO_CONTENT")




#projects api endpoints

class getProjects(APIView):
    def get(self,request, pk):
        project_all = Projects.objects.filter(client=pk)
        project_ser = ProjectSerializer(project_all, many= True)
        return Response(project_ser.data)

    def post(self,request,pk):
        project_ser = ProjectSerializer(data=request.data)
        project_ser.client = Clients.objects.get(id=pk)
        if project_ser.is_valid():
            project_ser.save(client=Clients.objects.get(id=pk))        
        return Response(project_ser.data)      

    def get(self,request, pk):
        project_all = Projects.objects.get(pk=pk)
        project_ser = ProjectSerializer(project_all)
        return Response(project_ser.data)

