from http import client
from rest_framework.response import Response
from mains.models import Clients, Projects
from .serializers import ClientsSerializer, ClientSerializer
from rest_framework import viewsets
#Clients api endpoints
class Getclients(viewsets.ViewSet):
 def list(self,request):
    client = Clients.objects.all()
    serializer = ClientSerializer(client,many=True)
    return Response(serializer.data)
 
 def create(self,request):
    #client = Clients.objects.all()
    serializer = ClientSerializer(data=request.data)
    if serializer.is_valid():
         serializer.save()
         return Response({"msg":"created"})

#class getClientdet(RetrieveUpdateDestroyAPIView):
#class getClientdet(viewsets.ViewSet):
 def retrieve(self,request,pk=None):
         id=pk
         if id is not None:
          client = Clients.objects.get(id=id)
          serializer = ClientsSerializer(client)
          return Response(serializer.data)
         
 def update(self,request,pk):
         id=pk
         client = Clients.objects.get(pk=id)
         serializer = ClientSerializer(client,data=request.data)
         if serializer.is_valid():
           serializer.save()
           return Response({"msg":"updated"})
         
 def partial_update(self,request,pk):
         id=pk
         #self.initial_data = data
         client = Clients.objects.get(pk=id)
         serializer = ClientSerializer(client,data=request.data,partial=True)
         if serializer.is_valid():
           serializer.save()
         return Response({"msg":"parital updated"})
         
 def destroy(self,request,pk=None):
         id=pk
         client = Clients.objects.get(pk=id)
         client.delete()
         return Response({"msg":"Deleted"})


'''class getProjects(ListCreateAPIView):
        queryset = Projects.objects.filter()
        serializer_class = ProjectSerializer'''

    