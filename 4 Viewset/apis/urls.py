
from django.contrib import admin
from django.urls import path
from . import views
from django.urls import path,include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter

router=DefaultRouter()
router.register('getclients',views.Getclients,basename='clients')


urlpatterns = [
    path('clients/',include(router.urls)),
    ]
