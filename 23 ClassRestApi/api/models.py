from django.db import models

# Create your models here.
class Category(models.Model):
    id=models.IntegerField(primary_key=True)
    title=models.CharField(max_length=50)
    created_at=models.DateTimeField(auto_now=True)
    updated_at=models.DateTimeField(auto_now=True)

class Products(models.Model):
    id=models.IntegerField(primary_key=True)
    title=models.CharField(max_length=200)
    category=models.ForeignKey(Category, on_delete=models.CASCADE,related_name='cat')
    created_at=models.DateTimeField(auto_now=True)
    updated_at=models.DateTimeField(auto_now=True)
