from rest_framework import serializers
from .models import Category, Products


class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', "title", "created_at", "updated_at")


class ProductSerializer(serializers.ModelSerializer):
    # queryset = Category.objects.all()
    category = CategorySerializers()
    # category=category.data

    class Meta:
        model = Products
        fields = ('id', 'title', 'created_at', 'updated_at', 'category')
        # depth=1
