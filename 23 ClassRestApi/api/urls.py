from django.urls import path
from api import views

urlpatterns = [

    path('category/', views.GetCategory.as_view()),
    path('category/<int:pk>', views.CategoryDetails.as_view()),
    path('product/', views.GetProduct.as_view()),
    path('product/<int:pk>', views.GetProduct.as_view()),
]
