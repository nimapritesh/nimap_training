from django.shortcuts import render
from rest_framework.response import Response
from .serializers import CategorySerializers, ProductSerializer
from .models import Category, Products
from rest_framework import status
from rest_framework.views import APIView
from drf_spectacular.utils import extend_schema
from api.pagination import CustomPagination


class GetCategory(APIView):
    @extend_schema(responses=CategorySerializers)
    def get(self, request):
        category_data = Category.objects.all()
        category_filter = request.query_params.get("filter")
        if category_filter != None:
            category_data = Category.objects.filter(title=category_filter)
        paginator = CustomPagination()
        result_page = paginator.paginate_queryset(category_data, request)

        serializer = CategorySerializers(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    @extend_schema(
        request=CategorySerializers,
        responses=CategorySerializers)
    def post(self, request):
        category_serializer = CategorySerializers(data=request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            return Response(category_serializer.data, status=status.HTTP_201_CREATED)
        return Response(category_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategoryDetails(APIView):
    def get(self, request, pk):
        category_data = Category.objects.get(id=pk)
        category_serializer = CategorySerializers(category_data)
        return Response(category_serializer.data)

    @extend_schema(
        request=CategorySerializers,
        responses=CategorySerializers)
    def put(self, request, pk):
        category_update = Category.objects.get(id=pk)
        category_serializer = CategorySerializers(
            instance=category_update, data=request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            msg = {"message": "Updated Successfully"}
            return Response(msg)
        return Response(category_serializer.data)

    @extend_schema(
        request=None,
        responses=CategorySerializers)
    def patch(self, request, pk):
        category_update = Category.objects.get(id=pk)
        category_serializer = CategorySerializers(
            instance=category_update, data=request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            msg = {"message": "Partial Updated Successfully"}
            return Response(msg)
        return Response(category_serializer.data)

    @extend_schema(
        responses=CategorySerializers)
    def delete(self, request, pk):
        category_delete = Category.objects.get(id=pk)
        category_delete.delete()
        msg = {"message": "Deleted Successfully"}
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


class GetProduct(APIView):
    def get(self, request):
        product_get = Products.objects.all()
        Product_serializer = ProductSerializer(product_get, many=True)
        return Response(Product_serializer.data)

    def post(self, request):
        product_serializer = ProductSerializer(data=request.data)
        if product_serializer.is_valid():
            product_serializer.save()
            return Response(product_serializer.data, status=status.HTTP_201_CREATED)
        return Response(product_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
