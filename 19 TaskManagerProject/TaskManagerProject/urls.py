"""
URL configuration for TaskManager project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from task import views

urlpatterns = [
    path('admin/', admin.site.urls),
    #path('', views.getowner, name='home'),
    path('task', views.gettask, name='gettask'),
    path('task/<int:id>',views.getsubtask),
    path('create',views.createtask),
    path("update",views.update, name='update'),
    path('', views.home,name='home'),
    path('register/', views.registeruser,name='register'),
    path('login/', views.loginuser,name='login'),
    path('logout/', views.logoutuser,name='logout'),
    path('delete/<int:id>', views.delete),
    path('__debug__/', include('debug_toolbar.urls')),
    

]
