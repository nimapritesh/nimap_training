from django.shortcuts import render,redirect,HttpResponse
from task.models import Task,Subtask
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from .froms import CreateUserFrom
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_page

@cache_page(40)
def home(request):
    return render(request,'home.html')

@login_required(login_url='login')
def gettask(request):
    id=request.user.id
    get_task=Task.objects.filter(owner_id=id)
    page=Paginator(get_task,10)
    page_list = request.GET.get('page')
    page=page.get_page(page_list)
    context={'page':page}      
    return render (request,'list.html',context)

def getsubtask(request,id):
    if request.method=='GET':
        get_task=Task.objects.get(id=id)
        get_subtask=Subtask.objects.filter(task_id=id)
        subtask={'subtask':get_subtask,'task':get_task}
        return render(request,'subtask.html',subtask)
    if request.method=="POST":
        task=Task.objects.get(id=id)
        subtask_name=request.POST.get('subtask_name')
        subtask=Subtask(subtask_name=subtask_name,task_id=task)
        subtask.save()
        return redirect('gettask')

def createtask(request):
    if request.method=="POST":
        task_name=request.POST.get('task_name')
        status=request.POST.get('status')
        if status=='complete':
            status=1
        else:
            status=0
        id=request.user.id
        owner_instance=User.objects.get(id=id)
        data=Task(task_name=task_name,status=status,owner_id=owner_instance)
        data.save()
        return redirect("gettask")

    return render(request,'createtask.html')

def update(request):
    id=request.POST.get('id')
    task_name=request.POST.get('task_name')
    status=request.POST.get('status')
    task_instance=Task.objects.get(id=id)
    task_instance.task_name=task_name
    if status=='complete':
        task_instance.status=1
    else:
        task_instance.status=0
    task_instance.save()
    return redirect("gettask")

def delete(request, id):
  member = Task.objects.get(id=id)
  member.delete()
  return redirect('gettask')

def registeruser(request):
    if request.user.is_authenticated:
        return redirect('gettask')
    else:
        form=CreateUserFrom()
        if request.method=='POST':
         form=CreateUserFrom(request.POST)
         if form.is_valid():
            form.save()
            user=form.cleaned_data.get('username')
            messages.success(request,'Account Created Successfully for ' + user)
            return redirect('login')
    
        
    context={'form':form}
    return render(request,'register.html',context)
    
def loginuser(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method=='POST':
         username=request.POST.get('username')
         password=request.POST.get('password')

         user=authenticate(request, username=username,password=password)

         if user is not None:
            login(request,user)
            return redirect('home')
         else:
            messages.info(request,'username and password is incorrect')

    return render(request,'login.html')

def logoutuser(request):
    logout(request)
    return redirect('login')


