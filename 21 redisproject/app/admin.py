from django.contrib import admin
from app.models import Fruits
# Register your models here.
class ModelAdmin(admin.ModelAdmin):
    list_display = ['fruit_name','fruit_price']

admin.site.register(Fruits, ModelAdmin)
