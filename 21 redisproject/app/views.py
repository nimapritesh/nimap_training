# from django.shortcuts import render
from django.core.cache import cache
from app.models import Fruits
from django.http import JsonResponse


def home(request):
    payload = []
    db = None
    if cache.get('fruits'):
        payload = cache.get('fruits')
        db = 'redis'
        print(cache.ttl('fruits'))
    else:
        objs = Fruits.objects.all()
        for obj in objs:
            payload.append(obj.fruit_name)
        db = 'sqllite'
        cache.set('fruits', payload, timeout=15)
    return JsonResponse({'status': 200, 'db': db, 'data': payload})
