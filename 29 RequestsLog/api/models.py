import json
from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Category(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)


class Product(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=200)
    categorys = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name='product', null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class RequestLog(models.Model):
    id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField(default=False, null=True)
    request_method = models.CharField(max_length=10)
    request_path = models.CharField(max_length=500)
    response_status = models.IntegerField()
    request_body = models.JSONField(default=dict, help_text=(
        "Semi-structured data in JSON Format"), null=True)
    remote_address = models.CharField(max_length=100)
    server_hostname = models.CharField(max_length=200)
    run_time = models.DecimalField(max_digits=23, decimal_places=20, default=0)
    timestamp = models.DateTimeField(auto_now=True)
