import socket
import time
import json
from .models import RequestLog


class RequestLogMiddleware():
    def __init__(self, get_response):
        self.get_response = get_response
        self.start_time = time.time()
        """
        request.body indicating the raw data included 
        in the http request body
        code and values assigns to request.req_body from request.body
        added new attribute req_body request.body ka values isme assign karega
        It allows the middleware to access and
        process the request body after the view function has been executed.
        the process_request method checks if the request method is 'POST', 'PUT', or 'PATCH' and, 
        if so, stores the request body data in a 
        separate attribute (request.req_body) of the request object.
        """

    def process_request(self, request):
        if request.method in ['POST', 'PUT', 'PATCH']:
            request.req_body = request.body
        return request

    def __call__(self, request):
        request = self.process_request(request)  # before view after
        response = self.get_response(request)
        self.process_response(request, response)  # after view
        return response

    def process_response(self, request, response):
        request_path = request.get_full_path()
        if not request_path.startswith('/api'):
            return response
        request_data = None
        content_type = request.META.get('CONTENT_TYPE',  'application/json')
        request_type = request.META.get('HTTP_ACCEPT', content_type)
        if request.method in ['POST', 'PUT', 'PATCH']:
            if content_type == 'application/json':
                request_data = json.loads(request.req_body)
        data = {
            "user_id": request.user.pk,
            "request_method": request.method,
            "request_path": request_path,
            "response_status": response.status_code,
            "remote_address": request.META['REMOTE_ADDR'],
            "server_hostname": socket.gethostname(),
            "request_body": request_data,
            "run_time": time.time() - self.start_time
        }
        print(data)
        RequestLog.objects.create(
            user_id=request.user.pk,
            request_method=request.method,
            request_path=request_path,
            response_status=response.status_code,
            remote_address=request.META['REMOTE_ADDR'],
            server_hostname=socket.gethostname(),
            request_body=request_data,
            run_time=time.time() - self.start_time
        )
        return response
