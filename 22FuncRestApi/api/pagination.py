# from rest_framework.pagination import LimitOffsetPagination
# from rest_framework.pagination import CursorPagination
from rest_framework import pagination


class CustomPagination(pagination.PageNumberPagination):
    page_size = 2
    page_query_param = 'page'
    page_size_query_param = 'per_page'
    max_page_size = 10


# class MylimitoffsetPagination(LimitOffsetPagination):
#     default_limit = 5
#     limit_query_param = "mylimit"  # limit ka mylimit
#     offset_query_param = "offsetmy"
#     max_limit = 4


# class MycursorPagination(CursorPagination):
#     page_size = 3
#     ordering = 'title'
#     cursor_query_param = 'cu'
