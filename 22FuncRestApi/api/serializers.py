from rest_framework import serializers
from api.models import Quotes, thought


class QuotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quotes
        fields = '__all__'


class ThoughtSerializer(serializers.ModelSerializer):
    class Meta:
        model = thought
        fields = '__all__'
