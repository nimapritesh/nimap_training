from django.shortcuts import render
from api.models import Quotes, thought
from rest_framework.response import Response
from api.serializers import QuotesSerializer, ThoughtSerializer
from rest_framework import status
from rest_framework.decorators import api_view, authentication_classes, permission_classes
# from api.pagination import MylimitoffsetPagination
from api.pagination import CustomPagination

# Create your views here


@api_view(['GET', 'POST'])
def getQuotes(request):
    if request.method == 'GET':
        qoutes_filter = request.query_params.get("filter")
        if qoutes_filter != None:
            quotes_data = Quotes.objects.filter(text=qoutes_filter)
        else:
            quotes_data = Quotes.objects.all()
        paginator = CustomPagination()
        result_page = paginator.paginate_queryset(quotes_data, request)

        serializer = QuotesSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        quotes_serializer = QuotesSerializer(data=request.data)
        print(quotes_serializer)
        if quotes_serializer.is_valid():
            quotes_serializer.save()
            msg = {"message": "Data Created Successfully"}
            return Response(msg)
        return Response(quotes_serializer.data)


@api_view(['GET', 'PUT', 'PATCH', 'DELETE'])
def quotes_details(request, pk):
    if request.method == 'GET':
        quotes_data = Quotes.objects.get(id=pk)
        quotes_serializer = QuotesSerializer(quotes_data)
        return Response(quotes_serializer.data)

    elif request.method == 'PUT':
        quotes_update = Quotes.objects.get(id=pk)
        quotes_serializer = QuotesSerializer(
            instance=quotes_update, data=request.data)
        print(quotes_serializer)
        if quotes_serializer.is_valid():
            quotes_serializer.save()
            msg = {"message": "Upadated Successfully"}
            return Response(msg)
        return Response(quotes_serializer)

    elif request.method == 'PATCH':
        quotes_update = Quotes.objects.get(id=pk)
        quotes_serializer = QuotesSerializer(
            instance=quotes_update, data=request.data)
        if quotes_serializer.is_valid():
            quotes_serializer.save()
            msg = {"message": "Partially Upadated Successfully"}
            return Response(msg)
        return Response(quotes_serializer)

    elif request.method == 'DELETE':
        quotes_delete = Quotes.objects.get(id=pk)
        quotes_delete.delete()
        msg = {'message': 'Deleted Successfully'}
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


# @api_view(['GET', 'POST'])
# def getQuotes(request):
#     if request.method == 'GET':
#         quotes_data = Quotes.objects.all()
#         quotes_serializer = QuotesSerializer(quotes_data, many=True)
#         # pagination_class = MylimitoffsetPagination
#         pagination_class = CustomPagination
#         return Response(quotes_serializer.data, status=status.HTTP_200_OK)
@api_view(['GET', 'POST'])
def thought_api(request):

    if request.method == 'GET':
        thouth_data = thought.objects.all()
        pagination = CustomPagination()
        thouht_page = pagination.paginate_queryset(thouth_data, request)

        serializer = ThoughtSerializer(thouht_page, many=True)
        # return Response(serializer.data)
        return pagination.get_paginated_response(serializer.data)

    elif request.method == 'POST':
        serializer = ThoughtSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
        return Response({"msg": "Message Added Succesfully"})
        # return Response(serializer.created_by)
