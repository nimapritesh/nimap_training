from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Quotes(models.Model):
    id = models.IntegerField(primary_key=True)
    text = models.TextField(max_length=150)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)


class thought(models.Model):
    title = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
