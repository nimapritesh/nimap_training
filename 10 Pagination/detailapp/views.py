from django.shortcuts import render
from .models import Person
from django.core.paginator import Paginator
# Create your views here.
def home(request):
    details=Person.objects.all()
    page=Paginator(details,10)
    # print(page.count)
    # print(page.num_pages)
    # print(page.page_range)
    # print(page.object_list)
    page_list=request.GET.get('page')
    page=page.get_page(page_list)
    data={'page':page}
    return render(request,'home.html',data)