from django.urls import path
from mainapp import views

urlpatterns = [
    path('registerapi/', views.register_user, name='register'),
    path('generate-password/', views.generate_password, name='generate-password')


]
