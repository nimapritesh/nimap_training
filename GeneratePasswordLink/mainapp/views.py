from django.utils import timezone
from datetime import timedelta
from django.shortcuts import render
from mainapp.task import send_generate_password_link
from django.contrib.auth.models import User
from mainapp.serializers import RegisterUserSerializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import GeneratePasswordCode


@csrf_exempt
@api_view(['GET', 'POST'])
def register_user(request):
    if request.method == "GET":
        user = User.objects.all()
        user_serializer = RegisterUserSerializers(user, many=True)
        return Response(user_serializer.data)

    elif request.method == "POST":
        email = request.data['email']
        user_serializer = RegisterUserSerializers(data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()
            send_generate_password_link.delay(
                email, user_serializer.data['id'])
            return JsonResponse(user_serializer.data)
        return JsonResponse({"Unable to post": "Kya Karu"})


@csrf_exempt
def generate_password(request):
    # print(request.POST.get("password"))
    token = request.GET.get('token')
    password = request.POST.get('password')
    obj = GeneratePasswordCode.objects.filter(token=token).first()

    if not obj:
        return JsonResponse({"msg": "Generate link is not valid"})
    elif obj.created_at < timezone.now() - timedelta(minutes=10):
        return JsonResponse({"msg": "Generate link expired."})

    user = obj.user
    password = make_password(password)
    print("password :", password)
    user.password = password
    user.set_password(password)
    user.save()
    return JsonResponse({"msg": "password change succesfully"})
