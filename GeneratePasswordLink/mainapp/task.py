from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core.mail import send_mail
from celery_project import settings
from celery import shared_task
import uuid
from mainapp.models import GeneratePasswordCode


@shared_task(bind=True)
def send_generate_password_link(self, email, user_id):
    tokencode=uuid.uuid4()
    instance=GeneratePasswordCode(token=tokencode,user_id=user_id)
    instance.save()
    reset_link = f'http://127.0.0.1:8000/api/generate-password/?token={tokencode}'
    mail_subject = "generate password"
    message = f"Click here to generate your password\n\n{reset_link}"    
    to_email = email
    send_mail(
        subject=mail_subject,
        message=message,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[to_email],
        fail_silently=True,
    )


