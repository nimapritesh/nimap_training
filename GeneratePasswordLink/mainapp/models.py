from django.db import models
import uuid
from django.contrib.auth.models import User

class GeneratePasswordCode(models.Model):
    token = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)