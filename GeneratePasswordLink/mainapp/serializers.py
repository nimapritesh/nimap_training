from rest_framework import serializers
from django.contrib.auth.models import User


class RegisterUserSerializers(serializers.ModelSerializer):
    # password = serializers.CharField(read_only=True)
    password = serializers.CharField(write_only=True,required=False)

    class Meta:
        model = User
        fields = ('id','username', 'email', 'password')
        
