from django.contrib import admin
from .models import Task,Subtask
# Register your models here.

class TaskAdmin(admin.ModelAdmin):
    list_display = ['owner_id','task_name','created_at','updated_at']
admin.site.register(Task, TaskAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ['subtask_name','created_at']
admin.site.register(Subtask, TaskAdmin)
