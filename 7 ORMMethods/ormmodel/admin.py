from django.contrib import admin
from .models import Album,Song
# Register your models here.
class MemberAdmin(admin.ModelAdmin):
    list_display=['id','title','artist','genre','awards']
admin.site.register(Album,MemberAdmin)

class MemberAdmin(admin.ModelAdmin):
    list_display=['id','name','album']
admin.site.register(Song,MemberAdmin)
