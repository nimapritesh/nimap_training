from django.db import models

# Create your models here.
class Album(models.Model):
    title = models.CharField(max_length = 30)
    artist = models.CharField(max_length = 30)
    genre = models.CharField(max_length = 30)
    awards=models.IntegerField(default=0)
  
    def __str__(self):
        return f"{self.title} {self.artist} {self.genre} {self.awards}"
  
class Song(models.Model):
    name = models.CharField(max_length = 100)
    album = models.ForeignKey(Album, on_delete = models.CASCADE)
  
    def __str__(self):
        return f"{self.name} {self.album}"