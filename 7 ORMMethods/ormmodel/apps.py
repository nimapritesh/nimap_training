from django.apps import AppConfig


class OrmmodelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ormmodel'
