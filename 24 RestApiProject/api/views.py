from django.shortcuts import render
from .models import Quotes, Category, Product, FAQS
from .serializers import CategorySerializer, ProductSerializer, QuotesSerializer, FaqsSerializers, RegisterUserSerializers
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from api.pagination import CustomPagination
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.views import APIView
from drf_spectacular.utils import extend_schema
from api.pagination import CustomPagination
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django.shortcuts import render
from api.task import send_mail_after_register, send_reset_password_link, send_change_password_email
from api.form import CreateUserForm
from django.shortcuts import render
from django.contrib.auth.models import User
from api.serializers import RegisterUserSerializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse


@api_view(['GET', 'POST'])
def getQuotes(request):
    if request.method == 'GET':
        quotes_data = Quotes.objects.all()
        qoutes_filter = request.query_params.get("filter")
        if qoutes_filter != None:
            quotes_data = Quotes.objects.filter(text=qoutes_filter)
        paginator = CustomPagination()
        result_page = paginator.paginate_queryset(quotes_data, request)

        serializer = QuotesSerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    if request.method == 'POST':
        quotes_serializer = QuotesSerializer(data=request.data)
        print(quotes_serializer)
        if quotes_serializer.is_valid():
            quotes_serializer.save()
            msg = {"message": "Data Created Successfully"}
            return Response(msg)
        return Response(quotes_serializer.data)


@api_view(['GET', 'PUT', 'PATCH', 'DELETE'])
def quotes_details(request, pk):
    if request.method == 'GET':
        quotes_data = Quotes.objects.get(id=pk)
        quotes_serializer = QuotesSerializer(quotes_data)
        return Response(quotes_serializer.data)

    elif request.method == 'PUT':
        quotes_update = Quotes.objects.get(id=pk)
        quotes_serializer = QuotesSerializer(
            instance=quotes_update, data=request.data)
        if quotes_serializer.is_valid():
            quotes_serializer.save()
            msg = {"message": "Upadated Successfully"}
            return Response(msg)
        return Response(quotes_serializer)

    elif request.method == 'PATCH':
        quotes_update = Quotes.objects.get(id=pk)
        quotes_serializer = QuotesSerializer(
            instance=quotes_update, data=request.data)
        if quotes_serializer.is_valid():
            quotes_serializer.save()
            msg = {"message": "Partially Upadated Successfully"}
            return Response(msg)
        return Response(quotes_serializer)

    elif request.method == 'DELETE':
        quotes_delete = Quotes.objects.get(id=pk)
        quotes_delete.delete()
        msg = {'message': 'Deleted Successfully'}
        return Response(msg, status=status.HTTP_204_NO_CONTENT)

# classbased view for category


class GetCategory(APIView):
    @extend_schema(responses=CategorySerializer)
    def get(self, request):
        category_data = Category.objects.all()
        category_filter = request.query_params.get("filter")
        if category_filter != None:
            category_data = Category.objects.filter(title=category_filter)
        paginator = CustomPagination()
        result_page = paginator.paginate_queryset(category_data, request)

        serializer = CategorySerializer(result_page, many=True)
        return paginator.get_paginated_response(serializer.data)

    @extend_schema(
        request=CategorySerializer,
        responses=CategorySerializer)
    def post(self, request):
        category_serializer = CategorySerializer(data=request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            return Response(category_serializer.data, status=status.HTTP_201_CREATED)
        return Response(category_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategoryDetails(APIView):
    # authentication_classes = [JWTAuthentication]
    # permission_classes = [DjangoModelPermissions]
    # queryset = Category.objects.none()

    def get(self, request, pk):
        category_data = Category.objects.get(id=pk)
        category_serializer = CategorySerializer(category_data)
        return Response(category_serializer.data)

    @extend_schema(
        request=CategorySerializer,
        responses=CategorySerializer)
    def put(self, request, pk):
        category_update = Category.objects.get(id=pk)
        category_serializer = CategorySerializer(
            instance=category_update, data=request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            msg = {"message": "Updated Successfully"}
            return Response(msg)
        return Response(category_serializer.data)

    @extend_schema(
        request=None,
        responses=CategorySerializer)
    def patch(self, request, pk):
        category_update = Category.objects.get(id=pk)
        category_serializer = CategorySerializer(
            instance=category_update, data=request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            msg = {"message": "Partial Updated Successfully"}
            return Response(msg)
        return Response(category_serializer.data)

    @extend_schema(
        responses=CategorySerializer)
    def delete(self, request, pk):
        category_delete = Category.objects.get(id=pk)
        category_delete.delete()
        msg = {"message": "Deleted Successfully"}
        return Response(msg, status=status.HTTP_204_NO_CONTENT)


# Generic views for product
class ProductListCreateApiView(ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    ordering_fields = ['title']
    search_fields = ['title']
    filterset_fields = ['id']
    # authentication_classes = [JWTAuthentication]
    # permission_classes = [DjangoModelPermissions]

    # def get_queryset(self):
    #     return Category.objects.filter(id=2)


class ProductRetriveUpdate(RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


# Here'e Starting File Upload and Cache

class ViewFaqs(ListCreateAPIView):
    queryset = FAQS.objects.all()
    serializer_class = FaqsSerializers

    @method_decorator(cache_page(300))
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class UpdateFaqs(RetrieveUpdateDestroyAPIView):
    queryset = FAQS.objects.all()
    serializer_class = FaqsSerializers


# Started Celery Views
@api_view(['GET', 'POST'])
def register_user(request):
    if request.method == "GET":
        user = User.objects.all()
        user_serializer = RegisterUserSerializers(user, many=True)
        return Response(user_serializer.data)

    elif request.method == "POST":
        email = request.data['email']
        password = request.data['password']
        request.data['password'] = make_password(password)
        user_serializer = RegisterUserSerializers(data=request.data)

        if user_serializer.is_valid():
            user_serializer.save()
            send_mail_after_register.delay(email)
            return JsonResponse(user_serializer.data)


@api_view(['GET', 'POST'])
def reset_password_link(request):
    email = request.GET.get('email')
    user = User.objects.get(email=email)
    send_reset_password_link.delay(email)
    return Response("sent")


@csrf_exempt
def change_password(request):
    email = request.GET.get('email')
    password = request.POST.get('password')
    user = User.objects.get(email=email)

    password = make_password(password)
    user.password = password
    user.save()
    send_change_password_email.delay(email)
    return JsonResponse({"msg": "password change succesfully"})


def signup_mail(request):

    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            send_mail_after_register.delay()
    return render(request, 'register.html', {'form': form})


# def send_mail_to_all(request):
#     send_mail_to_all.delay()
#     return HttpResponse("Sent Succesfully")
