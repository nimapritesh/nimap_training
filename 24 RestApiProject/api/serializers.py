from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Quotes, Category, Product, FAQS


class QuotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quotes
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    # categorys = CategorySerializer(read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        depth = 1


class CategorySerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = '__all__'

        depth = 1


class FaqsSerializers(serializers.ModelSerializer):
    class Meta:
        model = FAQS
        fields = "__all__"


class RegisterUserSerializers(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
