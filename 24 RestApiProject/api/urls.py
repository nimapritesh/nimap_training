from django.urls import path
from api import views


urlpatterns = [
    path('quotes/', views.getQuotes, name='quotes'),
    path('quotes/<int:pk>', views.quotes_details, name='quotedetails'),
    path('category/', views.GetCategory.as_view()),
    path('category/<int:pk>', views.CategoryDetails.as_view()),
    path('product/', views.ProductListCreateApiView.as_view()),
    path('product/<int:pk>', views.ProductRetriveUpdate.as_view()),
    path('faqs/', views.ViewFaqs.as_view()),
    path('faqsupdate/<int:pk>', views.UpdateFaqs.as_view()),
    # path('sendmail/', views.send_mail_to_all, name='sendmail'),
    path('register/', views.signup_mail, name='register'),
    path('registerapi/', views.register_user, name='register'),
    path('resetpassword/', views.reset_password_link, name='resetpassword'),
    path('changepassword/', views.change_password, name='changepassword')

]
