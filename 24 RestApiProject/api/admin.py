from django.contrib import admin
from .models import Quotes, Category, Product, FAQS, RequestLog
# Register your models here.


class QuotesAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'created_at', 'updated_at')


admin.site.register(Quotes, QuotesAdmin)


class Admin(admin.ModelAdmin):
    list_display = ['id', 'title', 'created_at', 'updated_at']


admin.site.register(Category, Admin)


class Admin(admin.ModelAdmin):
    list_display = ['id', 'title', 'created_at', 'updated_at']


admin.site.register(Product, Admin)


class FaqsAdmin(admin.ModelAdmin):
    list_display = ('id', 'questions', 'answer', 'attachment')


admin.site.register(FAQS, FaqsAdmin)


class RequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'user_id', 'request_method',
                    'request_path', 'response_status', 'request_body', 'remote_address', 'server_hostname', 'timestamp']


admin.site.register(RequestLog, RequestAdmin)
