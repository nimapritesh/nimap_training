
from django.contrib import admin
from django.urls import path,include
from . import views
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf import settings

if settings.DEBUG:
    import debug_toolbar


urlpatterns = [
    path('clients/', views.Getclients.as_view(), name='getClients'),
    path('clients/<int:pk>/', views.Clientdetail.as_view(), name='getClientdet'),
    path('clients/<int:pk>/projects', views.Getprojects.as_view(), name='getProjects'),
    path('__debug__/', include('debug_toolbar.urls')),
    
 ]
