from http import client
from mains.models import Clients, Projects
from .serializers import ProjectSerializer, ClientsSerializer, ClientSerializer
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView

#Clients api endpoints
class Getclients(ListCreateAPIView):
    queryset = Clients.objects.all()
    serializer_class = ClientSerializer

class Clientdetail(RetrieveUpdateDestroyAPIView):
        queryset = Clients.objects.all()
        serializer_class = ClientsSerializer


class Getprojects(ListCreateAPIView):
        queryset = Projects.objects.all()
        serializer_class = ProjectSerializer

    