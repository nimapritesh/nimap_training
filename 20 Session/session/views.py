from django.shortcuts import render,redirect

def setsession(request):
    request.session['name'] = 'Ritesh'
    request.session.set_expiry(0)
    return render(request,"session.html")

def getssion(request):
    name=request.session['name']
    # print(request.session.get_session_cookie_age())
    # print(request.session.get_expiry_age())
    # print(request.session.get_expiry_date())
    # print(request.session.get_expire_at_browser_close())
    # keys=request.session.keys()
    # items=request.session.items()
    # age=request.session.setdefault('age','22')
    return render(request,'getsession.html',{'data':name})

# def delsession(request):
#     if 'name' in request.session:
#      del request.session['name']
#     return render(request,'del.html')

def delsession(request):
     request.session.flush()
     request.session.clear_expired()
     return render(request,'del.html')