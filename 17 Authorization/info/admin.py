from django.contrib import admin
from .models import StudentData,Tasks
# Register your models here.
class StudentAdmin(admin.ModelAdmin):
    list_display = ['id','stu_name','stu_division','stu_stream']

admin.site.register(StudentData, StudentAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ('id','task_name','task_date')

admin.site.register(Tasks, TaskAdmin)
