from django.shortcuts import render,redirect
from .models import StudentData,Tasks
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required,permission_required

def registeruser(request):
   if request.user.is_authenticated:
        return render(request,'home.html',{'name':request.user.username})
   form=CreateUserForm()
   if request.method=='POST':
      form=CreateUserForm(request.POST)
      if form.is_valid():
         form.save()
         user=form.cleaned_data.get('username')
         messages.success(request,'Account Created Successfully for ' + user)
         return redirect('login')
   context={'form':form}
   return render(request,'register.html',context)

def userlogin(request):
   if request.user.is_authenticated:
        return redirect('home')
   if request.method=='POST':
      username=request.POST.get('username')
      password=request.POST.get('password')
      
      user=authenticate(request,username=username,password=password)
      if user is not None:
         login(request,user)
         return redirect('/')
      else:
         messages.info(request,'username and password is incorrect')
    
   return render(request,'login.html')

def logoutuser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
def homedetail(request):
    data=StudentData.objects.all()
    context={'data':data}
    return render(request,'home.html',context)

# def add(request):
#   return render(request,'add.html')
  
# def addrecord(request):
#   stu_name = request.POST['stu_name']
#   stu_division = request.POST['stu_division']
#   stu_stream=request.POST['stu_stream']
#   member = StudentData(stu_name=stu_name, stu_division=stu_division,stu_stream=stu_stream)
#   member.save()
#   return redirect('/')

def add(request):
    if request.method=="POST":
        stu_name=request.POST.get('stu_name')
        stu_division=request.POST.get('stu_division')
        stu_stream=request.POST.get('stu_stream')
        data=StudentData(stu_name=stu_name,stu_division=stu_division,stu_stream=stu_stream)
        data.save()
        return redirect('/')

    return render(request,'add.html')

@login_required(login_url='login')
def delete(request, id):
  member = StudentData.objects.get(id=id)
  member.delete()
  return redirect('/')

@login_required(login_url='login')
def update(request, id):
  data = StudentData.objects.get(id=id)
  context = {
    'data': data,
  }
  return render(request,'update.html',context)
  
def updaterecord(request, id):
  name = request.POST['name']
  division = request.POST['division']
  stream=request.POST['stream']
  data = StudentData.objects.get(id=id)
#   data= StudentData(stu_name=name,stu_division=division,stu_stream=stream)
  data.stu_name=name
  data.stu_division = division
  data.stu_stream= stream
  data.save()
  return redirect('/')

def createtask(request):
    if request.method=="POST":
        task_name=request.POST.get('task_name')
        task_date=request.POST.get('task_date')
        data=Tasks(task_name=task_name,task_date=task_date)
        data.save()
        return redirect('createtask')

    return render(request,'createtask.html')

def tasklist(request):
   gettask=Tasks.objects.all()
   print(gettask)
   context={'getlist':gettask}
   # return redirect('createtask')
   return render(request,'viewtask.html',context)