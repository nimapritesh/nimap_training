from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class StudentData(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    stu_name=models.CharField(max_length=30)
    stu_division=models.CharField(max_length=30)
    stu_stream=models.CharField(max_length=20)
    
class Tasks(models.Model):
    task_name=models.CharField(max_length=100)
    task_date=models.DateField(auto_now=True)

