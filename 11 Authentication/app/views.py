from django.shortcuts import render,redirect
from .models import Person
from django.core.paginator import Paginator
#from django.contrib.auth.forms import UserCreationForm
from .froms import CreateUserFrom
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required


def registeruser(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form=CreateUserFrom()
        if request.method=='POST':
         form=CreateUserFrom(request.POST)
         if form.is_valid():
            form.save()
            user=form.cleaned_data.get('username')
            messages.success(request,'Account Created Successfully for ' + user)
            return redirect('login')
    
        
    context={'form':form}
    return render(request,'register.html',context)
    
def loginuser(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method=='POST':
         username=request.POST.get('username')
         password=request.POST.get('password')

         user=authenticate(request, username=username,password=password)

         if user is not None:
            login(request,user)
            return redirect('home')
         else:
            messages.info(request,'username and password is incorrect')

    return render(request,'login.html')

def logoutuser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
def home(request):
    details=Person.objects.all()
    page=Paginator(details,10)
    page_list=request.GET.get('page')
    page=page.get_page(page_list)
    data={'page':page}
    return render(request,'home.html',data)