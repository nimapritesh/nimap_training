from django.contrib import admin
from .models import Person
# Register your models here.
class ModelAdmin(admin.ModelAdmin):
    list_display = ['id','first_name','last_name','age','address']

admin.site.register(Person, ModelAdmin)
