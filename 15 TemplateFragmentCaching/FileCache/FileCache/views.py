from django.shortcuts import render

def home(request):
    name='Ritesh'
    age=22
    company='Nimap'
    location='Lower Parel'
    context={'name':name,
             'age':age,
             'company':company,
             'location':location}
    return render(request,'home.html',context)

def skills(request):
    #skill1='python'
    skill2='java'
    skill3='sql'
    detail={
            'sk2':skill2,
            'sk3':skill3,}
    return render(request,'skill.html',detail)