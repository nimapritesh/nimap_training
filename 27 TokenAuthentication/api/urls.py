from django.urls import path
from api import views


urlpatterns = [
    path('category/', views.CatergoryListCreateApiView.as_view()),
    path('category/<int:pk>', views.CatergoryRetriveUpdate.as_view()),
    path('product/', views.ProductListCreateApiView.as_view()),
    path('product/<int:pk>', views.ProductRetriveUpdate.as_view()),
]
