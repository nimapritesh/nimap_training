from http import client
from django.shortcuts import render
from mains.models import Clients, Projects
from .serializers import ProjectSerializer, ClientsSerializer, ClientSerializer
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin,CreateModelMixin,RetrieveModelMixin,UpdateModelMixin,DestroyModelMixin


#Clients api endpoints

class Getclients(ListModelMixin,CreateModelMixin,GenericAPIView):
        queryset  = Clients.objects.all()
        serializer_class = ClientSerializer
        # client_ser = ClientListSerializer(queryset,many=True)
        def get(self,request,*args,**kwargs):
         return self.list(request,*args,**kwargs)
        
        def post(self,request,*args,**kwargs):
         return self.create(request,*args,**kwargs)


class Clientdetail(GenericAPIView,RetrieveModelMixin,UpdateModelMixin,DestroyModelMixin):
        queryset = Clients.objects.all()
        serializer_class= ClientsSerializer
        def get(self, request, *args, **kwargs):
         return self.retrieve(request, *args, **kwargs)

        def put(self, request, *args, **kwargs):
         return self.update(request, *args, **kwargs)

        def patch(self, request, *args, **kwargs):
         return self.partial_update(request, *args, **kwargs)

        def delete(self, request, *args, **kwargs):
         return self.destroy(request, *args, **kwargs)
        





class Projects(GenericAPIView, RetrieveModelMixin,CreateModelMixin,ListModelMixin):
        queryset = Projects.objects.all()
        serializer_class = ProjectSerializer
        def get(self, request, *args, **kwargs):
         return self.retrieve(request, *args, **kwargs)
        def post(self, request, *args, **kwargs):
         return self.create(request, *args, **kwargs)

# class Getprojectdet(GenericAPIView,RetrieveModelMixin,UpdateModelMixin,DestroyModelMixin):
#         queryset = Projects.objects.all()
#         serializer_class = ProjectSerializer(queryset,many=True)
        
#         def get(self, request, *args, **kwargs):
#          return self.retrieve(request, *args, **kwargs)

#         def put(self, request, *args, **kwargs):
#          return self.update(request, *args, **kwargs)
        
#         def delete(self, request, *args, **kwargs):
#          return self.destroy(request, *args, **kwargs)

