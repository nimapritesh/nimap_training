
from django.contrib import admin
from django.urls import path
from . import views
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    path('clients/', views.Getclients.as_view(), name='getClients'),
    path('clients/<int:pk>/', views.Clientdetail.as_view(), name='getClientdet'),
    path('clients/<int:pk>/projects', views.Projects.as_view(), name='getProjects'),
    
 ]
