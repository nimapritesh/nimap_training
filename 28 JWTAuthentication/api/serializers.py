from rest_framework import serializers
from rest_framework.utils import model_meta
from .models import Category, Product


class ProductSerializer(serializers.ModelSerializer):
    # categorys = CategorySerializer(read_only=True)

    class Meta:
        model = Product
        fields = '__all__'
        depth = 1


class CategorySerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = '__all__'

        depth = 1

    # def create(self, validated_data):
    #     cat = validated_data.pop('cat')
    #     category_data = Category.objects.create(**validated_data)
    #     return category_data

    # def create(self, validated_data):
    #     categories = validated_data.pop('cat')
    #     category = Product(**validated_data)
    #     # category.save()
    #     # for categories in categories:
    #     #     Product.objects.create(categorys=category, **categories)
    #     return category

    # def create(self, validated_data):
    #     print(validated_data)
    #     categories_data = validated_data.pop('categorys')
    #     products = Product.objects.create(**validated_data)
    #     for categorie_data in categories_data:
    #         Category.objects.create(categorys=products, **categorie_data)
    #     return products
