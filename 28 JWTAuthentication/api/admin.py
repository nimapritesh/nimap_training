from django.contrib import admin
from .models import Category, Product
# Register your models here.


class Admin(admin.ModelAdmin):
    list_display = ['id', 'title', 'created_at', 'updated_at']


admin.site.register(Category, Admin)


class Admin(admin.ModelAdmin):
    list_display = ['id', 'title', 'created_at', 'updated_at']


admin.site.register(Product, Admin)
