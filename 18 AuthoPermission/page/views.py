from django.shortcuts import render,redirect
from .models import Task
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required,permission_required

def registeruser(request):
   if request.user.is_authenticated:
        return render(request,'home.html',{'name':request.user.username})
   form=CreateUserForm()
   if request.method=='POST':
      form=CreateUserForm(request.POST)
      if form.is_valid():
         form.save()
         user=form.cleaned_data.get('username')
         messages.success(request,'Account Created Successfully for ' + user)
         return redirect('login')
   context={'form':form}
   return render(request,'register.html',context)

def userlogin(request):
   if request.user.is_authenticated:
        return redirect('home')
   if request.method=='POST':
      username=request.POST.get('username')
      password=request.POST.get('password')
      
      user=authenticate(request,username=username,password=password)
      if user is not None:
         login(request,user)
         return redirect('/')
      else:
         messages.info(request,'username and password is incorrect')
    
   return render(request,'login.html')

def logoutuser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
def homedetail(request):
    data=Task.objects.all()
    context={'data':data}
    return render(request,'home.html',context)


def update(request, id):
  data = Task.objects.get(id=id)
  context = {
    'data': data,
  }
  return render(request,'update.html',context)
  
def updaterecord(request, id):
  name = request.POST['task_name']
  data = Task.objects.get(id=id)
  data.task_name=name
  data.save()
  return redirect('/')

def createtask(request):
    if request.method=="POST":
        task_name=request.POST.get('task_name')
        user=request.POST.get('user')
        data=Task(task_name=task_name,user=user)
        data.save()
        return redirect('createtask')

    return render(request,'createtask.html')

def tasklist(request):
   gettask=Task.objects.all()
   print(gettask)
   context={'getlist':gettask}
   # return redirect('createtask')
   return render(request,'viewtask.html',context)